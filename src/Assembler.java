import java.util.HashMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Assembler {

	public static void main(String[] args) {

		HashMap<String, String> hashC = new HashMap<String, String>();
		hashC.put("D|M", "1010101");
		hashC.put("D&M", "1000000");
		hashC.put("M-D", "1000111");
		hashC.put("D-M", "1010011");
		hashC.put("D+M", "1000010");
		hashC.put("M-1", "1110010");
		hashC.put("M+1", "1110111");
		hashC.put("-M", "1110011");
		hashC.put("!M", "1110001");
		hashC.put("M", "1110000");
		hashC.put("D|A", "0010101");
		hashC.put("D&A", "0000000");
		hashC.put("A-D", "0000111");
		hashC.put("D-A", "0010011");
		hashC.put("D+A", "0000010");
		hashC.put("A-1", "0110010");
		hashC.put("D-1", "0001110");
		hashC.put("A+1", "0110111");
		hashC.put("D+1", "0011111");
		hashC.put("-A", "0110011");
		hashC.put("-D", "0001111");
		hashC.put("!A", "0110001");
		hashC.put("!D", "0001101");
		hashC.put("A", "0110000");
		hashC.put("D", "0001100");
		hashC.put("-1", "0111010");
		hashC.put("1", "0111111");
		hashC.put("0", "0101010");

		HashMap<String, String> hashD = new HashMap<String, String>();
		hashD.put(null, "000");
		hashD.put("AMD", "111");
		hashD.put("AD", "110");
		hashD.put("AM", "101");
		hashD.put("A", "100");
		hashD.put("MD", "011");
		hashD.put("D", "010");
		hashD.put("M", "001");

		HashMap<String, String> hashJ = new HashMap<String, String>();
		hashJ.put(null, "000");
		hashJ.put("JMP", "111");
		hashJ.put("JLE", "110");
		hashJ.put("JNE", "101");
		hashJ.put("JLT", "100");
		hashJ.put("JGE", "011");
		hashJ.put("JEQ", "010");
		hashJ.put("JGT", "001");

		File file = new File("test.asm");

		try {

			Scanner sc = new Scanner(file);
			String d = null;
			String c = null;
			String j = null;
			String bnum = null;
			while (sc.hasNextLine()) {

				String i = sc.nextLine().replaceAll("//.*", "");
				i = i.trim();

				

				if (i.contains("@")) {
					String foo = "";
					bnum = i.substring(i.indexOf("@") + 1);
					bnum = Integer.toBinaryString((Integer.parseInt(bnum)));

					for (int x = 0; x < 16 - bnum.length(); x++) {

						foo = foo.concat("0");
					}
					bnum = foo.concat(bnum);
					System.out.println(bnum);
				}

				if (i.contains(";")) {

					j = i.substring(i.indexOf(";") + 1, i.indexOf(";") + 4);

				}
				else
				{
					j = "000";
				}
				if (i.contains("=")) {
					if (i.contains("[")) 
					{
						if(i.indexOf("=")> i .indexOf("["))
						{
						String bar = "";
						bnum = i.substring(i.indexOf("[") + 1, i.indexOf("]"));
						bnum = Integer.toBinaryString((Integer.parseInt(bnum)));
						for (int x = 0; x < 16 - bnum.length(); x++) 
						{
							bar = bar.concat("0");
						}
						bnum = bar.concat(bnum);
						System.out.println(bnum);
						d = i.substring(0, i.indexOf("[") + 1);
						c = i.substring(i.indexOf("=") + 1);
						d = d.replace(d, "M");
						}
						else
						{
						
						String baz = "";
						bnum = i.substring(i.indexOf("[") + 1, i.indexOf("]"));
						bnum = Integer.toBinaryString((Integer.parseInt(bnum)));

						for (int x = 0; x < 16 - bnum.length(); x++) 
						{

							baz = baz.concat("0");
						}
						bnum = baz.concat(bnum);
						System.out.println(bnum);
					
						c = i.substring(i.indexOf("=") + 1, i.indexOf("]"));
						c = c.replace(c, "M");
					} 
					}
					else 
					{
						d = i.substring(0, i.indexOf("="));
						c = i.substring(i.indexOf("=") + 1);
					}
					if (i.contains(";")) 
					{
						c = i.substring(i.indexOf("=") + 1, i.indexOf(";"));
					}

				
					

				}
				if (c != null && j != null && d != null) {

					System.out.println("111" + getValue(hashD, d, "000") + getValue(hashC, c, "")
							+ getValue(hashJ, j, "000"));

				}

			}

			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private static String getValue(HashMap<String, String> map, String key, String defaultValue) {
		if (map.containsKey(key)) {
			return map.get(key);
		}
		return defaultValue;
	}
}